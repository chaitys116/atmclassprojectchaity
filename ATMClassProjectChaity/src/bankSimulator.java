//Sabrina Chaity
//CUS 1156
//ATM Project
//Professor Bukhari

import java.util.Scanner;
public class bankSimulator {

	
	public static void main (String[] args){

	Scanner In = new Scanner(System.in);
   
	int customerCardNumber = 12419; //Customer account number
	int customerCardPin = 824;    // Customer PIN number
	double accountBalance = 0; // Available balance
    
	// Loop unitl the user cancels 
	while(true){

		System.out.print("Please enter account number: ");
		int cardNum = In.nextInt();

		System.out.print("Please enter PIN number: ");
		int pinNum = In.nextInt();

		//Checking customers status
		boolean  ifClientExists = false;    

		//Condition to check if account and pin number match
		if(cardNum == customerCardNumber && pinNum == customerCardPin ){
			ifClientExists = true;
			System.out.println("Welcome to St. John's University Bank......");       
			System.out.print("A. Checking \n"
							+ "B. Saving\n"
							+ "C. Quit :\n"
							+ "Please Select Account Type : ");
			String AccType = In.next();
		if(AccType.equalsIgnoreCase("C")){
			System.out.println("Thank You And Have A Good Day!");
			break;
		}           
		}
		// If account and pin  number are correct, show customer account details
		if(ifClientExists){
			System.out.println("Account Balance : " + accountBalance);
			System.out.println("************************************");
      
			// Customer transaction
       	while(true){
       		System.out.println("A Deposit "
       						+ "\nB Withdraw "
       						+ "\nC Cancel ");
       		String askUser = In.next();
       		if(askUser.equalsIgnoreCase("B")){
       			System.out.println("Enter Withdrawal Ammount");
       			double ammount = In.nextDouble();
       			accountBalance -= ammount;
       			System.out.println("Account Balance After Transaction : " + accountBalance);
            }
       		if(askUser.equalsIgnoreCase("A")){
       			System.out.println("Enter Deposit Ammount");
       			double ammount = In.nextDouble();
       			accountBalance += ammount;
       			System.out.println("Account Balance After Transaction : " + accountBalance);
       		}
       		if(askUser.equalsIgnoreCase("C")){
       			System.out.println("Thank You And Have A Good Day!");
       			break;
       		}
       	}
       	break;
        }
		else{
			System.out.println("Invalid Account Number or Pin. Please try again!");
		}
}
}
}